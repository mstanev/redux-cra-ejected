import React from 'react';
import { connect } from 'react-redux';
import { IntlProvider } from 'react-intl';
import messages from './component-test-i18n';

import {
  simpleAction as callToAction,
  loadUser
} from './component-test-redux';
import { changeLanguage } from '../../actions/simpleAction';
import TestComponents from './component-test-component';

const TestContainer = (props) => {
  let { lang, changeLanguage } = props;

  return (
    <IntlProvider locale={lang} messages={messages[lang]}>
      <button onClick={() => changeLanguage('en')}>Change Lang {lang}</button>
      <TestComponents />
    </IntlProvider>
  )
}

const mapStateToProps = state => ({
  ...state.config,
  ...state.simpleReducer,
  ...state.componentTestReducer,
});
const mapDispatchToProps = {
  callToAction,
  loadUser,
  changeLanguage
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TestContainer);
