import React, { Component } from 'react';
import { connect } from 'react-redux';

import logo from './logo.svg';
import './DashboardContainer.css';
import { simpleAction } from '../actions/simpleAction';
import {
  simpleAction as callToAction,
  loadUser
} from './component-test/component-test-redux';


class DashboardContainer extends Component {
  constructor(props) {
    super(props);
    this.callToAction = this.callToAction.bind(this);
  }

  callToAction(e) {
    e.preventDefault();
    // this.props.simpleAction(e.clientX);
    this.props.callToAction();
  }

  render() {
    const { simpleAction, loadUser } = this.props;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <pre>
          {
            JSON.stringify(this.props)
          }
        </pre>
        {/* <button onClick={this.simpleAction}>Test redux action</button> */}
        <button onClick={e => simpleAction(e.clientX) && loadUser('milenstanev')}>Test redux action</button>
        <button onClick={this.callToAction}>Test callToAction</button>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        {/* <TestContainer data={[1, 2, 3, 4, 5]} /> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ...state.config,
  ...state.simpleReducer,
  ...state.componentTestReducer,
});
const mapDispatchToProps = {
  simpleAction,
  callToAction,
  loadUser
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardContainer);