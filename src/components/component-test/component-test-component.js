import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import ListComponent from '../../common-components/list-component';

const TestComponent = ({ data }) => <div>
  <h4>Title</h4>
  <FormattedMessage id="simple" />
  <br />
  <FormattedMessage id="placeholder" values={{ name: 'John' }} />
  <br />
  <FormattedMessage id="date" values={{ ts: Date.now() }} />
  <br />
  <FormattedMessage id="time" values={{ ts: Date.now() }} />
  <br />
  <FormattedMessage id="number" values={{ num: Math.random() * 1000 }} />
  <br />
  <FormattedMessage id="plural" values={{ num: 1 }} />
  <br />
  <FormattedMessage id="plural" values={{ num: 99 }} />
  <br />
  <FormattedMessage id="select" values={{ gender: 'male' }} />
  <br />
  <FormattedMessage id="select" values={{ gender: 'female' }} />
  <br />
  <FormattedMessage id="selectordinal" values={{ order: 1 }} />
  <br />
  <FormattedMessage id="selectordinal" values={{ order: 2 }} />
  <br />
  <FormattedMessage id="selectordinal" values={{ order: 3 }} />
  <br />
  <FormattedMessage id="selectordinal" values={{ order: 4 }} />
  <br />
  <FormattedMessage
    id="richtext"
    values={{ num: 99, bold: (...chunks) => <strong>{chunks}</strong> }}
  />
  <ListComponent data={data} />
</div>

TestComponent.propTypes = {
  data: PropTypes.array
}

export default TestComponent
