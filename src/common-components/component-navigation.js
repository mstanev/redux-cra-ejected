import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const Navigation = ({ classname }) => (
  <Fragment>
    <Link to='/'>App</Link>
    <span>   </span>
    <Link to='/test'>test</Link>
  </Fragment>
);

export default Navigation;