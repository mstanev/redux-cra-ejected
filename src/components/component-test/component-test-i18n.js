export default {
  en: {
    simple: 'Hello world',
    placeholder: 'Hello {name}',
    date: 'Hello {ts, date}',
    time: 'Hello {ts, time}',
    number: 'Hello {num, number}',
    plural: 'I have {num, plural, one {# dog} other {# dogs}}',
    select: 'I am a {gender, select, male {boy} female {girl}}',
    selectordinal: `I am the {order, selectordinal, 
        one {#st person} 
        two {#nd person}
        =3 {#rd person} 
        other {#th person}
    }`,
    richtext: 'I have <bold>{num, plural, one {# dog} other {# dogs}}</bold>',
  },
  de: {
    simple: 'Hello world = de',
    placeholder: 'Hello {name}  = de',
    date: 'Hello {ts, date}  = de',
    time: 'Hello {ts, time}  = de',
    number: 'Hello {num, number}  = de',
    plural: 'I have {num, plural, one {# dog} other {# dogs}}  = de',
    select: 'I am a {gender, select, male {boy} female {girl}}  = de',
    selectordinal: `I am the {order, selectordinal, 
        one {#st person} 
        two {#nd person}
        =3 {#rd person} 
        other {#th person}
    }  = de`,
    richtext: 'I have <bold>{num, plural, one {# dog} other {# dogs}}</bold>  = de',
  }
};