import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Layout from '../common-components/layout-component/component-layout';
import RooterComponent from './router/index';
import Navigation from '../common-components/component-navigation';

const App = () => (
  <BrowserRouter>
    <Layout>
      <Navigation />
      <RooterComponent />
    </Layout>
  </BrowserRouter>
);

export default App;