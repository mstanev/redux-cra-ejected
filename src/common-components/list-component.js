import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

const ListItemElement = ({ collection }) => collection.map((item, key) => <li key={key}>{item}</li>);
const NoItemsElement = ({ text }) => <li>{text || 'NoData'}</li>;

const ListComponent = ({ data }) => {
  return (
    <Fragment>
      <ul>
        {
          (data && data.length) ? <ListItemElement collection={data} /> : <NoItemsElement />
        }
      </ul>
    </Fragment>
  )
}

ListComponent.prototype = {
  data: PropTypes.array.isRequired
};

export default ListComponent;