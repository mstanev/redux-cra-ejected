import React from 'react'
import { shallow, mount, render, configure } from 'enzyme';
import sinon from 'sinon';
import DashboardContainer from './DashboardContainer';

import Adapter from 'enzyme-adapter-react-16';
import store from '../store';
configure({ adapter: new Adapter() });


const jsdom = require('jsdom');
const doc = jsdom.jsdom('<!doctype html><html><body></body></html>')
global.document = doc
global.window = doc.defaultView;

describe('Dashboard Component Test', () => {
  let dashboard;

  beforeEach(() => {
    // dashboard = shallow(<DashboardContainer store={store} />).instance();
    dashboard = mount(<DashboardContainer store={store} />).instance();
  });

  describe('Component Actions', () => {
    /*it('action ....', () => {
      dashboard.selector.props.callToAction();
      expect(dashboard.selector.props.counter).toBe(1);
    });*/

    it('simulates click events', () => {
      // const onButtonClick = sinon.spy();
      // const button = dashboard.find('button').eq(0);
      const { store } = dashboard.store;
      const initialStateJSON = JSON.stringify(store.getState());
      // button.simulate('click');
      dashboard.selector.props.callToAction();
      const updatedStateJSON = JSON.stringify(store.getState());

      expect(initialStateJSON !== updatedStateJSON).toBe(true);
    });
  })
})
