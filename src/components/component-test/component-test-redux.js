import { CALL_API } from '../../middleware/api'

// #region Action Types 
export const actionTypes = {
  DEFAULT_TEST_ACTION: 'DEFAULT_TEST_ACTION',
  USER_REQUEST: 'USER_REQUEST',
  USER_SUCCESS: 'USER_SUCCESS',
  USER_FAILURE: 'USER_FAILURE',
};
// #endregion

// #region Actions
export const simpleAction = payload => {
  return {
    type: actionTypes.DEFAULT_TEST_ACTION,
    payload
  };
}
/**
 * @param {string} login 
 */
export const loadUser = login => dispatch => {
  if (!login) {
    return dispatch({
      type: actionTypes.USER_FAILURE,
      error: `Login field is required` // intl key?
    });
  }

  const { USER_REQUEST, USER_SUCCESS, USER_FAILURE } = actionTypes;
  return dispatch(({
    [CALL_API]: {
      types: [USER_REQUEST, USER_SUCCESS, USER_FAILURE],
      endpoint: `users/${login}`
    }
  }));
}
// #endregion

// #region Reducers
export const defaultState = {
  counter: 0,
  user: {},
  fetchingUser: false,
  fetchingUserError: ''
}
/**
 * @ Reducer Description
 * @param {Object} state 
 * @param {Object} action
 */
export const componentTestReducer = (state = defaultState, action) => {
  switch (action.type) {
    case actionTypes.DEFAULT_TEST_ACTION:
      return {
        ...state,
        counter: ++state.counter
      }
    case actionTypes.USER_SUCCESS:
      return {
        ...state,
        user: action.response,
        fetchingUser: false
      }
    case actionTypes.USER_REQUEST:
      return {
        ...state,
        fetchingUser: true
      }
    case actionTypes.USER_FAILURE:
      return {
        ...state,
        fetchingUser: false,
        fetchingUserError: action.error
      }
    default:
      return state
  }
}
// #endregion