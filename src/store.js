import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import api from './middleware/api';
import { createLogger } from 'redux-logger';
import simpleReducer from './reducers/simpleReducer';
import { componentTestReducer } from './components/component-test/component-test-redux';

// #region Reducers injection
/**
 * @description Prepare reducers for for Redux store {StoreCreator}
 */
const combinedReducers = combineReducers({
  simpleReducer,
  componentTestReducer,
});
// #endregion


// #region Configure store
/**
 * @description Configure store and prepare an istance
 * @return store
 */
const configureStore = () => {
  let store;

  if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'test') {
    store = createStore(
      combinedReducers,
      applyMiddleware(thunk, api)
    );
  } else {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    store = createStore(
      combinedReducers,
      composeEnhancers(
        applyMiddleware(thunk, api, createLogger())
      )
    );
  }

  return store;
}
// #endregion


export default configureStore();