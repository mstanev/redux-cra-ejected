const initialState = {
  API_ROOT: 'https://api.github.com/',
  result: '',
  lang: 'de'
};

/* 
  src/reducers/simpleReducer.js
*/
export default (state = { ...initialState }, action) => {
  switch (action.type) {
    case 'SIMPLE_ACTION':
      return {
        ...state,
        result: action.payload
      }
    case 'RESET_ERROR_MESSAGE':
      return {
        ...state,
        error: 'noope'
      }
    case 'CHANGE_LANGUAGE':
      return {
        ...state,
        lang: action.payload
      }
    default:
      return state
  }
}
