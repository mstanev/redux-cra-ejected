import React from 'react';
import './component-layout.css';

const style = {
  row: {
    minHeight: "100vh"
  }
};

const Layout = ({ children }) => {
  const navigation = children[0];
  const main = children[1];

  return (
    <div className='container-fluid'>
      <div id='layout-wrapper' className='row' style={style.row}>
        <aside className='col-2'>
          <nav>
            {navigation}
          </nav>
        </aside>
        <section className='col-10'>
          {main}
        </section>
      </div>
    </div>
  )
};

export default Layout;