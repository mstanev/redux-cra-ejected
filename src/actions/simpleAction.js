/* 
  src/actions/simpleAction.js
*/
export const simpleAction = (payload) => ({
  type: 'SIMPLE_ACTION',
  payload: `result_of_simple_action ${Math.random()} ${payload}`
})

export const changeLanguage = payload => ({
  type: 'CHANGE_LANGUAGE', payload
})

export const resetErrorMessage = () => ({
  type: 'RESET_ERROR_MESSAGE'
})