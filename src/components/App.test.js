import React from 'react'
import { shallow, mount, render, configure } from 'enzyme';
import App from './App';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe('App Component Test', () => {
  let app = shallow(<App />);

  describe('Component Structure', () => {
    it('should render App', () => {
      expect(app).toBeDefined();
    })

    it('should render Layout', () => {
      expect(app.find('Layout')).toBeDefined();
    })

    it('should render Navigation', () => {
      expect(app.find('Navigation')).toBeDefined();
    })

    it('should render RooterComponent', () => {
      expect(app.find('RooterComponent')).toBeDefined();
    })
  });

});
