import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';

import DashboardContainer from '../DashboardContainer';
import Test from '../component-test/component-test-container';

const RooterComponent = () => (
  <Fragment>
    <Route path="/" component={DashboardContainer} />
    <Route path="/test" component={Test} />
  </Fragment>
);

export default RooterComponent;